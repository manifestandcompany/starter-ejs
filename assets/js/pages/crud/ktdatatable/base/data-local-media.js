'use strict';
// Class definition

var KTDatatableDataLocalDemo = function() {
    // Private functions

    // demo initializer
    var demo = function() {
        var dataJSONArray = jQuery('<p></p>').html(tableData1).text()
        dataJSONArray = dataJSONArray.replace(/\n/g,'')

        dataJSONArray = JSON.parse(dataJSONArray)
        let entities = dataJSONArray.entities

        let kdt_options = {
            // datasource definition
            data: {
                type: 'local',
                source: entities,
                pageSize: 10,
            },

            // layout definition
            layout: {
                scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
                // height: 450, // datatable's body's fixed height
                footer: false, // display/hide footer
            },

            // column sorting
            sortable: false,

            pagination: true,

            // columns definition
            columns: [{
                field: 'id',
                title: '#',
                width: 10,
                type: 'number',
                selector: {
                    class: ''
                },
                textAlign: 'center',
            }, {
                field: 'post',
                title: locale_post,
                template: function(row) {
                    return row.post.Title
                }
            },
            {
                field: 'media',
                title: locale_media,
                width:400,
                template: function(row) {

                    let src = API_URL+"/getDownloadUrl?file="+row.URL

                    if(row.Type.indexOf("image")>-1){
                        return '<img class="mb-3" style="max-width: 400px" src="'+src+'" />'
                    }
                    else if(row.Type.indexOf("video")>-1){
                        return  '<video class="mb-3" width="400" height="300" controls ><source src="'+src+'"></video>'
                    }
                    return ''
                }
            },
            {
                field: 'Actions',
                title: locale_actions,
                sortable: false,
                width:100,
                overflow: 'visible',
                autoHide: false,
                template: function(row) {

                    let actions = ''


                    let delete_action = '\
                    <a href="javascript:;" onClick="deleteMedia('+row.id+')" class="btn btn-icon btn-light btn-hover-primary btn-sm ml-1" title="'+locale_delete+'">\
                        <span class="svg-icon svg-icon-danger svg-icon-2x">\
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">\
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">\
                                    <rect x="0" y="0" width="24" height="24"/>\
                                    <path d="M6,8 L6,20.5 C6,21.3284271 6.67157288,22 7.5,22 L16.5,22 C17.3284271,22 18,21.3284271 18,20.5 L18,8 L6,8 Z" fill="#000000" fill-rule="nonzero"/>\
                                    <path d="M14,4.5 L14,4 C14,3.44771525 13.5522847,3 13,3 L11,3 C10.4477153,3 10,3.44771525 10,4 L10,4.5 L5.5,4.5 C5.22385763,4.5 5,4.72385763 5,5 L5,5.5 C5,5.77614237 5.22385763,6 5.5,6 L18.5,6 C18.7761424,6 19,5.77614237 19,5.5 L19,5 C19,4.72385763 18.7761424,4.5 18.5,4.5 L14,4.5 Z" fill="#000000" opacity="0.3"/>\
                                </g>\
                            </svg>\
                        </span>\
                    </a>\
                    ';

                    actions = actions+delete_action;

                    return actions

                },
            }],
        }

        var datatable = $('#kt_datatable').KTDatatable(kdt_options);

        //when some row is checked or unchecked
        datatable.on(
            'datatable-on-check datatable-on-uncheck',
            function(e) {
                var checkedNodes = datatable.rows('.datatable-row-active').nodes();
                var count = checkedNodes.length;
                if (count > 0) {
                    $('#kt_datatable_group_action_form').collapse('show');
                } else {
                    $('#kt_datatable_group_action_form').collapse('hide');
                }
        });

        $("#delete_selected_medias").click(function(){

            var ids = [];
            datatable.rows('.datatable-row-active').
            nodes().
            find('.checkbox > [type="checkbox"]').
            map(function(i, chk) {
                ids.push(parseInt($(chk).val()));
            });
            
            if(ids.length){
                //if some ids are selected

                let map = {
                    ids:ids
                }

                let jsonmap = JSON.stringify(map)

                Swal.fire({
                    title: locale_are_you_sure,
                    text: locale_you_wont_be_able_to_revert_this,
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: locale_yes_delete_selected
                  }).then((result) => {
                    if (result.value) {
                        
                        //show progress on button
                        $("#delete_selected_medias").html('<div class="spinner-border"></div>');

                        jQuery.ajax({
                            url:"/media/delete",
                            method: 'POST',
                            data: jsonmap,
                            cache:false,
                            contentType:'application/json',
                            dataType:'json',
                            success: function(ret_data) {
                                
                                if(!ret_data.status){

                                    Swal.fire({
                                        title:locale_oops,
                                        html:ret_data.message,
                                        icon:'error'
                                    })

                                }
                                else{

                                    //remove selected rows
                                    datatable.rows('.datatable-row-active').remove();
                                    //hide actions (that contain delete button)
                                    $('#kt_datatable_group_action_form').collapse('hide');

                                }

                            },
                            error: function (jqXHR, status,ret_data) {
                                Swal.fire({
                                    title:locale_oops,
                                    html:locale_internal_server_error_please_contact_support,
                                    icon:'error'
                                })
                            }
                        }).always(function (ret_data) {
                            //show progress on button
                            $("#delete_selected_medias").html(locale_delete_selected);
                        });
            
                    }
                })

            }

        });

    };

    return {
        // Public functions
        init: function() {
            // init dmeo
            demo();
        },
    };
}();

jQuery(document).ready(function() {
    KTDatatableDataLocalDemo.init();
});
