'use strict';
// Class definition

var KTDatatableDataLocalDemo = function() {
    // Private functions

    // demo initializer
    var demo = function() {
        let dataJSONArray = jQuery('<p></p>').html(tableData1).text()
        dataJSONArray = dataJSONArray.replace(/\n/g,'')

        dataJSONArray = JSON.parse(dataJSONArray)
        
        let entities = dataJSONArray.entities

        let kdt_options = {
            // datasource definition
            data: {
                type: 'local',
                source: entities,
                pageSize: 10,
            },

            // layout definition
            layout: {
                scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
                // height: 450, // datatable's body's fixed height
                footer: false, // display/hide footer
            },

            // column sorting
            sortable: true,

            pagination: true,

            search: {
                input: $('#kt_datatable_search_query'),
                key: 'generalSearch'
            },

            // columns definition
            columns: [{
                field: 'id',
                title: '#',
                width: 10,
                type: 'number',
                selector: {
                    class: ''
                },
                textAlign: 'center',
                sortable: false,
            },
            {
                field: 'title',
                title: locale_title,
                sortable: true,
            },
            {
                field:'thumbnail',
                title:locale_thumbnail,
                template:function(row){
                    
                    //we have to show first image here, of 50 x 50
                    let url = ''
                    if(row.hasOwnProperty('media')){
                        let media = row.media

                        if(media.length){

                            media=media.reverse();  

                            for(let i=0;i<media.length;i++){

                                if(media[i].Type.indexOf("image")>-1){
                                    url = media[i].URL
                                    break;
                                }

                            }

                        }
                    }
                    
                    //if we have got a URL, then let's show it

                    if(url){

                        let src = API_URL+"/getDownloadUrl?file="+url

                        return '<img class="mb-3" style="max-width: 50px" src="'+src+'" />'

                    }

                    return ''
                    
                },
                sortable: false,
            },
            {
                field:'categories',
                title:locale_categories,
                template:function(row){
                    if(row.hasOwnProperty('categories') && row.categories.length){
                        return row.categories.join(", ")
                    }
                    return ''
                },
                sortable: true,
            },
            {
                field:'UserName',
                title:locale_author,
                sortable: true,
            },
            {
                field:'PublishedAt',
                title:locale_published_on,
                template: function(row) {

                    let date = ''

                    if(row.PublishedAt){
                        let published_at_splitted = row.PublishedAt.split("T")
                        date = published_at_splitted[0]
                    }

                    return date; 
                }
            },
            // {
            //     field: 'description',
            //     title: locale_description,
            //     template: function(row) {
            //         return $('<textarea />').html(row.description).text().subString(0,4)
            //     },
            // },
            {
                field: 'status',
                title: locale_status,
                sortable: true,
            }, {
                field: 'Actions',
                title: locale_actions,
                sortable: false,
                overflow: 'visible',
                autoHide: false,
                template: function(row) {

                    let actions = ''

                    let edit_action = '\
                    <div class="dropdown dropdown-inline">\
                    <a href="/posts/'+row.id+'/edit" class="mt-2 btn btn-icon btn-light btn-hover-primary btn-sm ml-1" title="'+locale_edit+'">\
                        <span class="svg-icon svg-icon-primary svg-icon-2x">\
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">\
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">\
                                    <rect x="0" y="0" width="15" height="15"/>\
                                    <path d="M12.2674799,18.2323597 L12.0084872,5.45852451 C12.0004303,5.06114792 12.1504154,4.6768183 12.4255037,4.38993949 L15.0030167,1.70195304 L17.5910752,4.40093695 C17.8599071,4.6812911 18.0095067,5.05499603 18.0083938,5.44341307 L17.9718262,18.2062508 C17.9694575,19.0329966 17.2985816,19.701953 16.4718324,19.701953 L13.7671717,19.701953 C12.9505952,19.701953 12.2840328,19.0487684 12.2674799,18.2323597 Z" fill="#000000" fill-rule="nonzero" transform="translate(14.701953, 10.701953) rotate(-135.000000) translate(-14.701953, -10.701953) "/>\
                                    <path d="M12.9,2 C13.4522847,2 13.9,2.44771525 13.9,3 C13.9,3.55228475 13.4522847,4 12.9,4 L6,4 C4.8954305,4 4,4.8954305 4,6 L4,18 C4,19.1045695 4.8954305,20 6,20 L18,20 C19.1045695,20 20,19.1045695 20,18 L20,13 C20,12.4477153 20.4477153,12 21,12 C21.5522847,12 22,12.4477153 22,13 L22,18 C22,20.209139 20.209139,22 18,22 L6,22 C3.790861,22 2,20.209139 2,18 L2,6 C2,3.790861 3.790861,2 6,2 L12.9,2 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>\
                                </g>\
                            </svg>\
                        </span>\
                    </a>\
                    ';
                    actions = actions+edit_action;

                    let delete_action = '\
                    <a href="javascript:;" onClick="deletePost('+row.id+')" class="mt-2 btn btn-icon btn-light btn-hover-primary btn-sm ml-1" title="'+locale_delete+'">\
                        <span class="svg-icon svg-icon-danger svg-icon-2x">\
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">\
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">\
                                    <rect x="0" y="0" width="24" height="24"/>\
                                    <path d="M6,8 L6,20.5 C6,21.3284271 6.67157288,22 7.5,22 L16.5,22 C17.3284271,22 18,21.3284271 18,20.5 L18,8 L6,8 Z" fill="#000000" fill-rule="nonzero"/>\
                                    <path d="M14,4.5 L14,4 C14,3.44771525 13.5522847,3 13,3 L11,3 C10.4477153,3 10,3.44771525 10,4 L10,4.5 L5.5,4.5 C5.22385763,4.5 5,4.72385763 5,5 L5,5.5 C5,5.77614237 5.22385763,6 5.5,6 L18.5,6 C18.7761424,6 19,5.77614237 19,5.5 L19,5 C19,4.72385763 18.7761424,4.5 18.5,4.5 L14,4.5 Z" fill="#000000" opacity="0.3"/>\
                                </g>\
                            </svg>\
                        </span>\
                    </a>\
                    ';

                    actions = actions+delete_action;

                    let update_status_actions = '\
                    <a href="/posts/updateStatus/'+row.id+'/verify" class="mt-2 btn btn-icon btn-light btn-hover-primary btn-sm ml-1" title="'+locale_verify+'">\
                        <span class="svg-icon svg-icon-success svg-icon-2x">\
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">\
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">\
                                <polygon points="0 0 24 0 24 24 0 24"/>\
                                <path d="M6.26193932,17.6476484 C5.90425297,18.0684559 5.27315905,18.1196257 4.85235158,17.7619393 C4.43154411,17.404253 4.38037434,16.773159 4.73806068,16.3523516 L13.2380607,6.35235158 C13.6013618,5.92493855 14.2451015,5.87991302 14.6643638,6.25259068 L19.1643638,10.2525907 C19.5771466,10.6195087 19.6143273,11.2515811 19.2474093,11.6643638 C18.8804913,12.0771466 18.2484189,12.1143273 17.8356362,11.7474093 L14.0997854,8.42665306 L6.26193932,17.6476484 Z" fill="#000000" fill-rule="nonzero" transform="translate(11.999995, 12.000002) rotate(-180.000000) translate(-11.999995, -12.000002) "/>\
                            </g>\
                            </svg>\
                        </span>\
                    </a>\
                    <a href="/posts/updateStatus/'+row.id+'/reject" class="mt-2 btn btn-icon btn-light btn-hover-primary btn-sm ml-1" title="'+locale_reject+'">\
                        <span class="svg-icon svg-icon-info svg-icon-2x">\
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">\
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">\
                                    <g transform="translate(12.000000, 12.000000) rotate(-45.000000) translate(-12.000000, -12.000000) translate(4.000000, 4.000000)" fill="#000000">\
                                        <rect x="0" y="7" width="16" height="2" rx="1"/>\
                                        <rect opacity="0.3" transform="translate(8.000000, 8.000000) rotate(-270.000000) translate(-8.000000, -8.000000) " x="0" y="7" width="16" height="2" rx="1"/>\
                                    </g>\
                                </g>\
                            </svg>\
                        </span>\
                    </a>\
                    ';

                    if(row.status==="publish"){

                        //put reject, verify only if post is published

                        actions = actions+update_status_actions;
                    }

                    let view_action = '\
                    <a href="/posts/'+row.id+'/view" class="mt-2 btn btn-icon btn-light btn-hover-primary btn-sm ml-1" title="'+locale_view+'">\
                        <span class="svg-icon svg-icon-info svg-icon-2x">\
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">\
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">\
                                    <rect x="0" y="0" width="24" height="24"/>\
                                    <path d="M3,12 C3,12 5.45454545,6 12,6 C16.9090909,6 21,12 21,12 C21,12 16.9090909,18 12,18 C5.45454545,18 3,12 3,12 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"/>\
                                    <path d="M12,15 C10.3431458,15 9,13.6568542 9,12 C9,10.3431458 10.3431458,9 12,9 C13.6568542,9 15,10.3431458 15,12 C15,13.6568542 13.6568542,15 12,15 Z" fill="#000000" opacity="0.3"/>\
                                </g>\
                            </svg>\
                        </span>\
                    </a>\
                    <div>\
                    ';

                    actions = actions+view_action;

                    return actions

                },
            }],
        }

        var datatable = $('#kt_datatable').KTDatatable(kdt_options);

        datatable.on(
            'datatable-on-check datatable-on-uncheck',
            function(e) {
                var checkedNodes = datatable.rows('.datatable-row-active').nodes();
                var count = checkedNodes.length;
                if (count > 0) {
                    $('#kt_datatable_group_action_form').collapse('show');
                } else {
                    $('#kt_datatable_group_action_form').collapse('hide');
                }
        });

        $("#delete_selected_posts").click(function(){

            var ids = [];
            datatable.rows('.datatable-row-active').
            nodes().
            find('.checkbox > [type="checkbox"]').
            map(function(i, chk) {
                ids.push(parseInt($(chk).val()));
            });
            
            if(ids.length){
                //if some ids are selected

                let map = {
                    ids:ids
                }

                let jsonmap = JSON.stringify(map)

                Swal.fire({
                    title: locale_are_you_sure,
                    text: locale_you_wont_be_able_to_revert_this,
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: locale_yes_delete_selected
                  }).then((result) => {
                    if (result.value) {

                        //show progress on button
                        $("#delete_selected_posts").html('<div class="spinner-border"></div>');

                        jQuery.ajax({
                            url:"/posts/delete",
                            method: 'POST',
                            data: jsonmap,
                            cache:false,
                            contentType:'application/json',
                            dataType:'json',
                            success: function(ret_data) {
                                
                                if(!ret_data.status){

                                    Swal.fire({
                                        title:locale_oops,
                                        html:ret_data.message,
                                        icon:'error'
                                    })

                                }
                                else{

                                    //remove selected rows
                                    datatable.rows('.datatable-row-active').remove();
                                    //hide actions (that contain delete button)
                                    $('#kt_datatable_group_action_form').collapse('hide');

                                }

                            },
                            error: function (jqXHR, status,ret_data) {
                                Swal.fire({
                                    title:'Ooops!',
                                    html:locale_internal_server_error_please_contact_support,
                                    icon:'error'
                                })
                            }
                        }).always(function (ret_data) {
                            //show progress on button
                            $("#delete_selected_posts").html(locale_delete_selected);
                        });
            
                    }
                })

            }

        });

    };

    return {
        // Public functions
        init: function() {
            // init dmeo
            demo();
        },
    };
}();

jQuery(document).ready(function() {
    KTDatatableDataLocalDemo.init();
});
