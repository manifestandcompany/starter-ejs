$(function(){
    $('.select2').select2({
        placeholder: $(".select2").attr("placeholder"),
        allowClear: true,
        closeOnSelect: false
    });
});