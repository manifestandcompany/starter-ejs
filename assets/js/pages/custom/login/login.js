"use strict";

function validateLoginForm(){

    //lets check fields are not empty

    let number_pattern = /^\d+$/

    let validations_ok = true
    $("#public_key").removeClass("is-invalid");
    $("#private_key").removeClass("is-invalid");
    $("#module_id").removeClass("is-invalid");

    let public_key = $("#public_key").val().trim();
    let private_key = $("#private_key").val().trim();
    let module_id = $("#module_id").val().trim();

    if(public_key===''){
        validations_ok = false
        $("#public_key").addClass("is-invalid");
    }

    if(private_key===""){
        validations_ok = false
        $("#private_key").addClass("is-invalid");
    }

    if(module_id===""){
        validations_ok = false
        $("#module_id").addClass("is-invalid");
    }
    else if(!number_pattern.test(module_id)){
        validations_ok = false
        $("#module_id").addClass("is-invalid");
    }

    if(validations_ok){
        return true;
    }

    return false;
}