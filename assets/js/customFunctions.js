AWS.config.update({
    secretAccessKey: getCookie('s3_secretkey'),
    accessKeyId: getCookie('s3_accesskey'),
    region: getCookie('s3_region')
});

//when user changes app language, we need to set it in cookie
$("#app_language").change(function(){
    let lang = $(this).val().trim();

    //set our new lang in cookie for next 365 days
    setCookie('BLOGAPP_lang',lang,365);
    window.location.reload();
});

//on page load, we need to set lang cookie value in lang dropdown
//check if there is no cookie then set it as en
$(function(){

    let lang = CURRENT_LOCALE
    //if there is no setting, then set it as en(English)
    if(!lang){
        lang = "en"
    }

    setCookie('BLOGAPP_lang',lang,365)
    $("#app_language").val(lang)

});

function getCookie(name) {
    function escape(s) { return s.replace(/([.*+?\^${}()|\[\]\/\\])/g, '\\$1'); };
    var match = document.cookie.match(RegExp('(?:^|;\\s*)' + escape(name) + '=([^;]*)'));
    return match ? match[1] : null;
}

function setCookie(variable, value, expires_days) {
    var d = new Date();
    d.setDate(d.getDate() + expires_days);
    document.cookie = variable + '=' + value + '; expires=' + d.toGMTString() + ';';
}


$("#save_settings").click(function(){

    //lets get all the settings on the page
    let elements = $("#settings_row").find(".form-control");

    //clear all errors
    let validations_ok = true
    $("#settings_row .error_span").css("display","none");

    let data = []

    elements.each(function() {

        let inner_map = {
            Name:$(this).attr("id"),
            Value:$(this).val()
        }

        if($(this).val().trim()===""){
            validations_ok = false;
            $("#"+$(this).attr("id")+"_error").css('display',"")
        }

        data.push(inner_map)
        
    });

    if(validations_ok){
        //update settings in database

        let jsonmap = JSON.stringify(data)

        //show progress

        $("#save_settings").html('<div class="spinner-border"></div>');

        jQuery.ajax({
            url:"/apisettings/updateAll",
            method: 'POST',
            data: jsonmap,
            cache:false,
            contentType:'application/json',
            dataType:'json',
            success: function(ret_data) {

                if(ret_data.responseType==='danger'){
                    Swal.fire({
                        title: locale_error,
                        html: ret_data.responseMessage,
                        icon: 'error',
                    })
                }
                else{

                    Swal.fire({
                        title: locale_success,
                        html: ret_data.responseMessage,
                        icon: 'success',
                    })

                }

            },
            error: function (jqXHR, status,ret_data) {
            }
        }).always(function (ret_data) {
            $("#save_settings").html(locale_save);
        });
        
    }
    else{
        Swal.fire({
            title: locale_oops,
            html: locale_please_check_form_for_errors,
            icon: 'error',
        })
    }
})

$("#comment_status_filter").change(function(){
    let status = $(this).val();

    window.location.href = "/comments/"+status
})

$("#post_status_filter").change(function(){
    let status = $(this).val();
    let category = $("#post_category_filter").val();

    window.location.href = "/posts/"+status+"/"+category
})

$("#post_category_filter").change(function(){
    let category = $(this).val();
    let status = $("#post_status_filter").val();

    window.location.href = "/posts/"+status+"/"+category
    
})

$('#summernote').summernote({
    placeholder: locale_description+' . . . .',
    tabsize: 2,
    height: 100
});



//sumernote has an issue that, it's default writing highlighening color is in yellow, so we have manually set it to black
$('[data-original-title="Recent Color"]').each(function(  ) {
    $(this).attr("data-backcolor", "#FFFFFF")
    $(this).attr("style", "background-color: rgb(255 255 255);");
    $(this).find('i').attr('style',"color:#000000")
});

function formSubmit(val){
    $('#status').val(val);
    if(validateCreatePostForm(val)){
        $("#post_form").submit();
    }
    
}

function validateCreatePostForm(val){
    
    $("#submit_"+val).addClass('spinner spinner-primary spinner-left spinner-white');
    
    let title = $("#title").val().trim();
    let category = $("#category").val();
    
    let description = $("#summernote").val().trim();

    let files_added = $("#files_added_links").val();

    let validations_ok = true;
    $("#titleError").css("display","none");
    $("#descriptionError").css("display","none");
    $("#categoryError").css("display","none");

    if(title===""){
        validations_ok = false;
        $("#titleError").css("display","");
        $("#titleError").html(locale_title_required);
    }

    if((description=="" || description=='<p><br></p>' || description=="&nbsp;") && files_added===""){
        validations_ok = false;
        $("#descriptionError").css("display","");
        $("#descriptionError").html(locale_description_required);
    }

    if(!category.length){
        validations_ok = false;
        $("#categoryError").css("display","");
        $("#categoryError").html(locale_category_required);
    }

    $("#submit_"+val).removeClass('spinner spinner-primary spinner-left spinner-white');

    return validations_ok;

}

function fileSelected(e){

    if(e.currentTarget.files.length){
        
        for(let i=0;i<e.currentTarget.files.length;i++){

        $("#image_loader").removeAttr("style");//show the loader
        
            let file = e.currentTarget.files[i];
    
            let fileType = e.currentTarget.files[i].type;

            let name = e.currentTarget.files[i].name;

            let extension = name.split(".").pop();

            let key = Math.floor(new Date().getTime())+"."+extension;

            var albumBucketName = getCookie('s3_bucket')
            var upload = new AWS.S3.ManagedUpload({
                params: {
                Bucket: albumBucketName,
                Key: key,
                Body: file,
                ACL: getCookie('s3_acl')
                }
            });

            var promise = upload.promise();

            promise.then(
                async function(data) {

                   let url = data.Location

                let files_added_links_list = $("#files_added_links_list").val().trim();
                if(files_added_links_list!==""){
                    let fall = files_added_links_list.split(",");
                    fall.push(url)
                    $("#files_added_links_list").val(fall.join(","))
                }
                else{
                    $("#files_added_links_list").val(url)
                }

                //also we need to keep track of media types
                let files_added_links_types = $("#files_added_links_types").val().trim();
                if(files_added_links_types!==""){
                    let falt = files_added_links_types.split(",");
                    falt.push(fileType)
                    $("#files_added_links_types").val(falt.join(","))
                }
                else{
                    $("#files_added_links_types").val(fileType)
                }

                    let src = API_URL+"/getDownloadUrl?file="+url

                if(fileType.indexOf('image') === 0){
                        url = '<img class="mb-3" style="max-width: 500px" src="'+src+'" />'
                }
                else if(fileType.indexOf('video') === 0){
                        url = '<video class="mb-3" width="500" height="240" controls ><source src="'+src+'"></video>'
                }

                let files_added = $("#files_added").html()

                files_added = files_added+url+'<br/>'

                    $("#files_added").html(files_added);

                    $("#image_loader").attr("style","display:none");//hide the loader
       
            });

            }
       
    }
  
}

function deleteMedia(id){
    Swal.fire({
        title: locale_are_you_sure,
        html: locale_you_wont_be_able_to_revert_this,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: locale_yes_delete_it
      }).then((result) => {
        if (result.value) {
            
            window.location.href="/media/"+id+"/delete"

        }
    })
}

function deletePost(postid){
    Swal.fire({
        title: locale_are_you_sure,
        html: locale_you_wont_be_able_to_revert_this,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: locale_yes_delete_it
      }).then((result) => {
        if (result.value) {
            
            window.location.href="/posts/"+postid+"/delete"

        }
    })
}

function deleteComment(commentid){
    Swal.fire({
        title: locale_are_you_sure,
        html: locale_you_wont_be_able_to_revert_this,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: locale_yes_delete_it
      }).then((result) => {
        if (result.value) {
            
            window.location.href="/comments/"+commentid+"/delete"

        }
    })
}

function addCommentOnPost(postId){
    
    let comment = $("#comment").val().trim();

    if(comment && comment!==""){

        let map = {
            postId,comment
        }

        let jsonmap = JSON.stringify(map)

        //show progresss on button

        $("#create_comment").html('<div class="spinner-border"></div>');

        jQuery.ajax({
            url:"/comments/create",
            method: 'POST',
            data: jsonmap,
            cache:false,
            contentType:'application/json',
            dataType:'json',
            success: function(ret_data) {

                if(ret_data.responseType==='danger'){
                    Swal.fire({
                        title: locale_error,
                        html: ret_data.responseMessage,
                        icon: 'error',
                    })
                }
                else{

                    Swal.fire({
                        title: locale_success,
                        html: ret_data.responseMessage,
                        icon: 'success',
                    })

                    $("#comment").val('')

                }

            },
            error: function (jqXHR, status,ret_data) {
            }
        }).always(function (ret_data) {
            $("#create_comment").html(locale_save);
        });

    }
    else{
        Swal.fire({
            title: locale_error,
            html: locale_please_provide_comment_text,
            icon: 'error',
        })
    }

}