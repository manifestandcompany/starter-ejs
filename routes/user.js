const express = require('express')
const userController = require('../controllers/users')
const router = express.Router()

router.post('/users', userController.getAllUsers)
router.get('/users', userController.getAllUsers)

module.exports = router;