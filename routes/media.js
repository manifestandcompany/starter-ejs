const express = require('express')
const mediaController = require('../controllers/media')
const router = express.Router()

router.get('/media', mediaController.getAllMedia)

router.get('/media/:id/delete', mediaController.deleteMedia)
router.post('/media/delete', mediaController.deleteMedias)

module.exports = router;