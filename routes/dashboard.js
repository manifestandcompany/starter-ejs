const express = require('express')
const dashboardController = require('../controllers/dashboard')
const router = express.Router()

router.post('/', dashboardController.dashboardPage)
router.get('/', dashboardController.dashboardPage)

module.exports = router;