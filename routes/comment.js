const express = require('express')
const commentsController = require('../controllers/comments')
const router = express.Router()

router.post('/comments/create', commentsController.createComment)

router.get('/comments/updateStatus/:id/:status', commentsController.updateStatus)

router.get('/comments/:id/delete', commentsController.deleteComment)
router.post('/comments/delete', commentsController.deleteComments)

router.get('/comments/:status?', commentsController.getAllComments)

module.exports = router;