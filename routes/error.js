const express = require('express')
const router = express.Router()

router.get('/error',(req,res) => {
    res.render('errors/error.ejs')
})

module.exports = router;