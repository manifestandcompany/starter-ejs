const express = require('express')
const postsController = require('../controllers/posts')
const router = express.Router()

router.get('/posts/updateStatus/:id/:status', postsController.updateStatus)

router.get('/posts/:id/edit', postsController.createNewPost)
router.post('/posts/:id/edit', postsController.createNewPost)

router.get('/posts/:id/view', postsController.viewPost)

router.get('/posts/:id/delete', postsController.deletePost)
router.post('/posts/delete', postsController.deletePosts)

router.get('/posts/new', postsController.createNewPost)
router.post('/posts/new', postsController.createNewPost)

router.post('/posts/:status?/:category?', postsController.getAllPosts)
router.get('/posts/:status?/:category?', postsController.getAllPosts)

// router.get('/reset', authController.getReset);
// router.post('/reset', authController.postReset);
// router.get('/reset/:token', authController.getNewPassword);
// router.post('/new-password', authController.postNewPassword);

module.exports = router;