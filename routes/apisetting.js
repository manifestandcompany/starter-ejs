const express = require('express')
const apisettingsController = require('../controllers/apisettings')
const router = express.Router()

router.get('/apisettings', apisettingsController.getSettings)
router.post('/apisettings/updateAll', apisettingsController.updateAllSettings)

module.exports = router;