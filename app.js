// ADD LIBRARIES
const path = require('path');
const express = require('express');
const config = require('./config')
const session = require('express-session');
const bodyParser= require('body-parser')
// ADD ROUTES
const dashboardRoutes = require('./routes/dashboard');
const postRoutes = require('./routes/post');
const commentRoutes = require('./routes/comment');
const mediaRoutes = require('./routes/media');
const errorRoutes = require('./routes/error');
const authRoutes = require('./routes/auth');

const userRoutes = require('./routes/user');

const apisettingRoutes = require('./routes/apisetting');
const cookieParser = require('cookie-parser');
const { I18n } = require('i18n')
 
const i18n = new I18n({
    locales: ['en', 'fr'],
    directoryPermissions: '755',
    defaultLocale: 'en',
    retryInDefaultLocale: true,
    cookie: 'BLOGAPP_lang',
    directory: path.join(__dirname, 'i18n')
})

// ADD AUTH CONTROLLER
const authController = require('./controllers/auth');

// SET PORT AND EXPRESS APP
const PORT = config.PORT;
const app = express();


// SET EJS ENGINE
app.set('view engine', 'ejs');

// SET VIEWS FOLDER
app.set('views', 'views');

//Cookies
app.use(cookieParser());

//use ie8n locales
app.use(i18n.init)

// SET BODY PARSER
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// SET SESSION FOR STORAGE
app.use(session({secret: config.SESSIONKEY, saveUninitialized: true, resave: true}))

app.use(express.static(path.join(__dirname, '/')));

app.use(errorRoutes);

// CHECK FROM PRE CONDITIONS
app.use(async (req, res, next) => {

    if(req.cookies['BLOGAPP_lang']){
        //on load, set your locale from cookie
        req.setLocale(req.cookies['BLOGAPP_lang']);
    }
    else{
        req.setLocale('en');
    }


    res.cookie('s3_acl', config.ACL)
    res.cookie('s3_secretkey', config.SECRETKEY)
    res.cookie('s3_accesskey', config.ACCESSKEY)
    res.cookie('s3_region', config.REGION)
    res.cookie('s3_bucket', config.BUCKETNAME)

    if(config.authentication_method==="CONFIG"){

        //we need to check if there is session then go to next, otherwise show login page so that users can provide app keys and can login

        if(req.session[config.SESSIONVAR+'_user']){
            //if session exists

            //if still login page is called, move them to dashboard
            if(req.path.indexOf('login')>-1){
                res.redirect("/");
            }
            else{
                next()
            }
            
        }
        else{

            //check if page is login then let them go to login, otherwise redirect to login

            if(req.path.indexOf('login')>-1){
                next()
            }
            else{
                res.redirect("/login");
            }
            
        }

    }
    else{
        //method POST, this method expects data to be sent from user module
        console.log(req.session)
    let check_params = false
    if(req.method==="POST" && req.body.hasOwnProperty('app_key_hash') && req.body.hasOwnProperty('module_id')){
        check_params = true
    }

        console.log(check_params)

        if(check_params){
            await authController.getPermissions(req,res)
            next()
        }
        else if(req.session[config.SESSIONVAR+'_user']){
        next()
    }
    else{

            console.log("reqsession")
            console.log(req.session)

            //this check tells us, neither there were params nor session exists, so user needs to login from user module
            res.redirect(config.USER_MODULE_URL);
        }

    }

});


// SET ROUTES
app.use(dashboardRoutes);

app.use(userRoutes);

app.use(postRoutes);
app.use(commentRoutes);
app.use(mediaRoutes);
app.use(apisettingRoutes);
app.use(authRoutes);

// app.get('/500', errorController.get500);

// app.use(errorController.get404);

// START SERVER 
app.listen(PORT, () => {
    console.log(`App listening on port ${PORT}.`);
});