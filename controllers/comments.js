const fetch = require("node-fetch");
const config = require('../config');
const auth_methods = require('../controllers/auth.js')

/**
 * GET ALL COMMENTS FROM API
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 * @param {render||return, default render} response_type 
 */

exports.getAllComments = async (req,res,next,response_type = "render") => {
    try{
        
        let status = "all"
        if(req.params.hasOwnProperty('status') && req.params.status!=""){
            status = req.params.status
        }

        // SEND AJAX CALL TO GET ALL comments
        let response = await fetch(config.API_URL+"/comments/get",{
            method:'POST',
            async:false,
            body:JSON.stringify({status}),
            headers: {'client_base_url':config.BASE_URL,'content-type': 'application/json', 'Authorization': 'Bearer '+req.session[config.SESSIONVAR+'_token'] }
        });
        
        //if we get 403, it means our session is expired, lets renew it
        if(response.status===403){

            await auth_methods.refreshPermissions(req,res)

            return await this.getAllComments(req, res, next, response_type);

        }

        // GET JSON DATA
        const ret_data = await response.json();

        if(response.status===200){
            if(response_type==="return"){
                return JSON.stringify(ret_data)
            }
            else{
                // SEND DATA WITH STATUS TO comments PAGE
                res.render('comments/all', {
                    status:status,
                    tableData: JSON.stringify(ret_data),
                    responseType:'success',
                    responseMessage:'',
                    user:req.session[config.SESSIONVAR+'_user'].user,
                    API_URL:config.API_URL
                });
            }
        }
        else{

            if(response_type==="return"){
                return JSON.stringify(ret_data)
            }
            else{

                let message = res.__('OPERATION_UNAUTHORIZED_MESSAGE');    
                message = message.replace("{$1}",res.__('id_read_capital'))

                res.render('dashboard', {
                    responseType:'danger',
                    responseMessage:message,
                    user:req.session[config.SESSIONVAR+'_user'].user,
                    API_URL:config.API_URL
                })
            }

        }


        
    }
    catch(error){
        console.log('error')
        console.log(error)
    }
}

/**
 * UPDATE COMMENT STATUS
 * @param {id, status} req 
 * @param {*} res 
 * @param {*} next 
 */

exports.updateStatus = async (req,res,next) => {

    try{
        let commentId = req.params.id
        let status = req.params.status

        if(commentId && status){

            let response = await fetch(config.API_URL+"/comments/udpateCommentStatus",{
                method:'POST',
                async:false,
                body:JSON.stringify({commentId:commentId,status:status}),
                headers: {'client_base_url':config.BASE_URL,'content-type': 'application/json', 'Authorization': 'Bearer '+req.session[config.SESSIONVAR+'_token'] }
            });

            let response_status = response.status

            //if we get 403, it means our session is expired, lets renew it
            if(response.status===403){

                await auth_methods.refreshPermissions(req,res)

                return await this.updateStatus(req, res, next);

            }

            let responseMessage = ''
            let responseType = ''

            if(response_status===200){
                responseMessage = res.__("Comment has been successfully updated to ")+res.__(status);
                responseType = 'success'
            }
            else{
                let message = res.__('OPERATION_UNAUTHORIZED_MESSAGE');    
                message = message.replace("{$1}",res.__('id_update_capital'))

                responseMessage = message
                responseType = 'danger'
            }

            req.params.status = "all"

            let comments = await this.getAllComments(req,res,next,'return')

            res.render('comments/all', {
                status:"all",
                tableData:comments,
                responseMessage: responseMessage,
                responseType: responseType,
                user:req.session[config.SESSIONVAR+'_user'].user,
                API_URL:config.API_URL
            });

        }
        else{
            req.params.status = "all"
            let comments = await this.getAllComments(req,res,next,'return')
            res.render('comments/all', {
                status:"all",
                tableData:comments,
                responseMessage: res.__('Could not get comment to update,invalid data sent'),
                responseType:"danger",
                user:req.session[config.SESSIONVAR+'_user'].user,
                API_URL:config.API_URL
            });
        }

    }
    catch(error){

        req.params.status = "all"
        let comments = await this.getAllComments(req,res,next,'return')

        res.render('comments/all', {
            status:"all",
            tableData:comments,
            responseMessage: res.__('Sorry, internal server error, please contact support'),
            responseType:"danger",
            user:req.session[config.SESSIONVAR+'_user'].user,
            API_URL:config.API_URL
        });
    }
}

/**
 * delete multiple comments physically
 * @param {ids} req 
 * @param {*} res 
 * @param {*} next 
 */

exports.deleteComments = async (req,res,next) => {
    try{

        let body = req.body

        if(body.hasOwnProperty('ids') && body.ids.length){
            let ids = req.body.ids

            let response = await fetch(config.API_URL+"/comments/deleteMultipleComments",{
                method:'POST',
                async:false,
                body:JSON.stringify({ids:ids}),
                headers: {'client_base_url':config.BASE_URL,'content-type': 'application/json', 'Authorization': 'Bearer '+req.session[config.SESSIONVAR+'_token'] }
            });
            
            let status = response.status

            let responseMessage = ''
            let responseStatus = ''

            //if we get 403, it means our session is expired, lets renew it
            if(response.status===403){

                await auth_methods.refreshPermissions(req,res)

                return await this.deleteComments(req, res, next);

            }

            if(status===200){
                responseMessage = res.__('Selected comments have been deleted successfully');
                responseStatus = true
            }
            else{

                let message = res.__('OPERATION_UNAUTHORIZED_MESSAGE');    
                message = message.replace("{$1}",res.__('id_delete_capital'))

                responseMessage = message
                responseStatus = false
            }

            res.send({
                status:responseStatus,
                message:responseMessage
            })

        }
        else{
            res.send({
                status:0,
                message:res.__('Invalid data sent, no records to delete')
            })
        }

    }
    catch(error){
        res.send({
            status:0,
            error:error.message,
            message:res.__('Internal server error, please contact support')
        })
    }
}

/**
 * delete single comment physically
 * @param {id} req 
 * @param {*} res 
 * @param {*} next 
 */

exports.deleteComment = async (req,res,next) => {
    try{
        
        let commentId = req.params.id

        if(commentId){
            let response = await fetch(config.API_URL+"/comments/delete",{
                method:'POST',
                async:false,
                body:JSON.stringify({commentId:commentId}),
                headers: {'client_base_url':config.BASE_URL,'content-type': 'application/json', 'Authorization': 'Bearer '+req.session[config.SESSIONVAR+'_token'] }
            });
            
            let status = response.status

            //if we get 403, it means our session is expired, lets renew it
            if(response.status===403){

                await auth_methods.refreshPermissions(req,res)

                return await this.deleteComment(req, res, next);

            }

            let responseMessage = ''
            let responseType = ''

            if(status===200){
                responseMessage = res.__('Comment has been deleted successfully');
                responseType = 'success'
            }
            else{
                let message = res.__('OPERATION_UNAUTHORIZED_MESSAGE');    
                message = message.replace("{$1}",res.__('id_delete_capital'))

                responseMessage = message
                responseType = 'danger'
            }

            req.params.status = "all"

            let comments = await this.getAllComments(req,res,next,'return')

            res.render('comments/all', {
                status:"all",
                tableData:comments,
                responseMessage: responseMessage,
                responseType: responseType,
                user:req.session[config.SESSIONVAR+'_user'].user,
                API_URL:config.API_URL
            });

        }
        else{

            req.params.status = "all"

            let comments = await this.getAllComments(req,res,next,'return')
            res.render('comments/all', {
                status:"all",
                tableData:comments,
                responseMessage: res.__('Invalid data sent, no records to delete'),
                responseType:"danger",
                user:req.session[config.SESSIONVAR+'_user'].user,
                API_URL:config.API_URL
            });
        }

    }
    catch(error){

        req.params.status = "all"

        let comments = await this.getAllComments(req,res,next,'return')

        res.render('comments/all', {
            status:"all",
            tableData:comments,
            responseMessage: res.__('Internal server error, please contact support'),
            responseType:"danger",
            user:req.session[config.SESSIONVAR+'_user'].user,
            API_URL:config.API_URL
        });
    }
}

/**
 * create single comment on a post
 * @param {postId, comment} req 
 * @param {*} res 
 * @param {*} next 
 */

exports.createComment = async (req,res,next) => {

    try{

        let postid = req.body.postId
        let comment = req.body.comment

        if(postid && comment){

            let post = {
                postId: postid,
                comment: comment,
                status: 'pending',
            }

            let response = await fetch(config.API_URL+"/comments/create",{
                method:'POST',
                async:false,
                body:JSON.stringify({post}),
                headers: {'client_base_url':config.BASE_URL,'content-type': 'application/json', 'Authorization': 'Bearer '+req.session[config.SESSIONVAR+'_token'] }
            });

            let response_status = response.status

            //if we get 403, it means our session is expired, lets renew it
            if(response.status===403){

                await auth_methods.refreshPermissions(req,res)

                return await this.createComment(req, res, next);

            }

            let responseMessage = ''
            let responseType = ''

            if(response_status===200){

                let message = res.__('COMMENT_CREATED_MESSAGE');    

                responseMessage = message;
                responseType = 'success'
            }
            else{
                let message = res.__('OPERATION_UNAUTHORIZED_MESSAGE');    
                message = message.replace("{$1}",res.__('id_create_capital'))

                responseMessage = message
                responseType = 'danger'
            }

            res.send({
                responseMessage,responseType
            })

        }
        else{

            let responseType = 'danger'

            let responseMessage = res.__("Could not create comment,invalid data sent")

            res.send({
                responseMessage,responseType
            })

        }

    }
    catch(error){

        let responseType = 'danger'

        let responseMessage = res.__('Internal server error, please contact support')

        res.send({
            responseMessage,responseType
        })
        
    }
}
