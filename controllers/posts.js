const fetch = require("node-fetch");
const config = require('../config');

const auth_methods = require('../controllers/auth.js')
const Entities = require('html-entities').AllHtmlEntities;
const entities_library = new Entities();
//https://manifest-articles-api2.herokuapp.com/
// POST OBJECT
let defaultPost = {
    id: '',
    title: '',
    description: '',
    categories:[],
    postcategories:[],
    status: '',
    form_action: '/posts/new',
    API_URL:config.API_URL
}

/**
 * GET ALL POST FROM SERVER AND SEND TO POST PAGE
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 * @param {*} response_type 
 */

exports.getAllPosts = async (req, res, next,response_type = "render") => {
    console.log('posts start')
    console.log(req.session)
    try{

        let status = "all"
        if(req.params.hasOwnProperty('status') && req.params.status!=""){
            status = req.params.status
        }

        let category = "all"
        if(req.params.hasOwnProperty('category') && req.params.category!=""){
            category = req.params.category
        }

        // SEND AJAX CALL TO GET ALL POSTS
        let response = await fetch(config.API_URL+"/posts/get",{
            method:'POST',
            async:false,
            body:JSON.stringify({category,status}),
            headers: {'client_base_url':config.BASE_URL,'content-type': 'application/json', 'Authorization': 'Bearer '+req.session[config.SESSIONVAR+'_token'] }
        });
        
        //if we get 403, it means our session is expired, lets renew it
        if(response.status===403){

            await auth_methods.refreshPermissions(req,res)

            return await this.getAllPosts(req, res, next, response_type);

        }

        // GET JSON DATA
        const ret_data = await response.json();

        if(response.status===200){
            //all went well
            let entities = ret_data.entities

            //description in entities is in html form, we need to encode it in htmlentities, so that we can send it to the client
            for(let i=0;i<entities.length;i++){

                entities[i].description = entities_library.encode(entities[i].description)

            }

            ret_data.entities = entities

            if(response_type==="return"){
                return JSON.stringify(ret_data)
            }
            else{
                console.log('posts return place')
                console.log(req.session)

                // SEND DATA WITH STATUS TO POSTS PAGE
                res.render('posts/all', {
                    status:status,
                    category:category,
                    tableData: JSON.stringify(ret_data),
                    responseType:'success',
                    responseMessage:'',
                    user:req.session[config.SESSIONVAR+'_user'].user,
                    categories:ret_data.categories,
                    API_URL:config.API_URL
                });
            }
        }
        else{
            //permission issues

            if(response_type==="return"){
                return JSON.stringify(ret_data)
            }
            else{
                
                let message = res.__('OPERATION_UNAUTHORIZED_MESSAGE');    
                message = message.replace("{$1}",res.__('id_read_capital'))
    
                res.render('dashboard', {
                    categories:[],
                    responseType:'danger',
                    responseMessage:message,
                    user:req.session[config.SESSIONVAR+'_user'].user,
                    API_URL:config.API_URL
                })
            }

        }

    }
    catch(error){
        console.log('error')
        console.log(error)
    }
};

/**
 * update post status
 * @param {postid,status} req 
 * @param {*} res 
 * @param {*} next 
 */

exports.updateStatus = async (req,res,next) => {

    try{
        let postid = req.params.id
        let status = req.params.status

        if(postid && status){

            let response = await fetch(config.API_URL+"/posts/updateStatus",{
                method:'POST',
                async:false,
                body:JSON.stringify({postId:postid,status:status}),
                headers: {'client_base_url':config.BASE_URL,'content-type': 'application/json', 'Authorization': 'Bearer '+req.session[config.SESSIONVAR+'_token'] }
            });

            let response_status = response.status

            let responseMessage = ''
            let responseType = ''

            //if we get 403, it means our session is expired, lets renew it
            if(response.status===403){

                await auth_methods.refreshPermissions(req,res)

                return await this.updateStatus(req, res, next);

            }

            if(response_status===200){
                responseMessage = res.__("Comment has been successfully updated to ")+res.__(status);
                responseType = 'success'
            }
            else{

                let message = res.__('OPERATION_UNAUTHORIZED_MESSAGE');    
                message = message.replace("{$1}",res.__('id_update_capital'))

                responseMessage = message
                responseType = 'danger'
            }

            req.params.status="all"
            req.params.category="all"

            let posts = await this.getAllPosts(req,res,next,'return')

            let post_response_json = JSON.parse(posts)

            res.render('posts/all', {
                status:"all",
                category:"all",
                tableData:posts,
                categories:post_response_json.categories,
                responseMessage: responseMessage,
                responseType: responseType,
                user:req.session[config.SESSIONVAR+'_user'].user,
                API_URL:config.API_URL
            });

        }
        else{

            req.params.status="all"
            req.params.category="all"

            let posts = await this.getAllPosts(req,res,next,'return')

            let post_response_json = JSON.parse(posts)

            res.render('posts/all', {
                status:"all",
                category:"all",
                tableData:posts,
                categories:post_response_json.categories,
                responseMessage: res.__("Could not get post to update,invalid data sent"),
                responseType:"danger",
                user:req.session[config.SESSIONVAR+'_user'].user,
                API_URL:config.API_URL
            });
        }

    }
    catch(error){

        req.params.status="all"
        req.params.category="all"

        let posts = await this.getAllPosts(req,res,next,'return')

        let post_response_json = JSON.parse(posts)

        res.render('posts/all', {
            status:"all",
            category:"all",
            tableData:posts,
            categories:post_response_json.categories,
            responseMessage: res.__("Internal server error, please contact support"),
            responseType:"danger",
            user:req.session[config.SESSIONVAR+'_user'].user,
            API_URL:config.API_URL
        });
    }
}

/**
 * delete multiple posts physically
 * @param {ids} req 
 * @param {*} res 
 * @param {*} next 
 */

exports.deletePosts = async (req,res,next) => {
    try{

        let body = req.body

        if(body.hasOwnProperty('ids') && body.ids.length){
            let ids = req.body.ids

            let response = await fetch(config.API_URL+"/posts/deleteMultiplePosts",{
                method:'POST',
                async:false,
                body:JSON.stringify({ids:ids}),
                headers: {'client_base_url':config.BASE_URL,'content-type': 'application/json', 'Authorization': 'Bearer '+req.session[config.SESSIONVAR+'_token'] }
            });
            
            let status = response.status

            let responseMessage = ''
            let responseStatus = ''

            //if we get 403, it means our session is expired, lets renew it
            if(response.status===403){

                await auth_methods.refreshPermissions(req,res)

                return await this.deletePosts(req, res, next);

            }

            if(status===200){
                responseMessage = res.__("Selected posts have been deleted successfully");
                responseStatus = true
            }
            else{
                let message = res.__('OPERATION_UNAUTHORIZED_MESSAGE');    
                message = message.replace("{$1}",res.__('id_delete_capital'))

                responseMessage = message
                responseStatus = false
            }

            res.send({
                status:responseStatus,
                message:responseMessage
            })

        }
        else{
            res.send({
                status:0,
                message:res.__('Invalid data sent, no records to delete')
            })
        }

    }
    catch(error){
        res.send({
            status:0,
            error:error.message,
            message:res.__('Internal server error, please contact support')
        })
    }
}

/**
 * delete single post physically
 * @param {id} req 
 * @param {*} res 
 * @param {*} next 
 */

exports.deletePost = async (req,res,next) => {
    try{
        
        let postid = req.params.id

        if(postid){
            let response = await fetch(config.API_URL+"/posts/delete",{
                method:'POST',
                async:false,
                body:JSON.stringify({postId:postid}),
                headers: {'client_base_url':config.BASE_URL,'content-type': 'application/json', 'Authorization': 'Bearer '+req.session[config.SESSIONVAR+'_token'] }
            });
            
            let status = response.status

            let responseMessage = ''
            let responseType = ''

            //if we get 403, it means our session is expired, lets renew it
            if(response.status===403){

                await auth_methods.refreshPermissions(req,res)

                return await this.deletePost(req, res, next);

            }

            if(status===200){
                responseMessage = res.__("Post has been deleted successfully");
                responseType = 'success'
            }
            else{
                let message = res.__('OPERATION_UNAUTHORIZED_MESSAGE');    
                message = message.replace("{$1}",res.__('id_delete_capital'))

                responseMessage = message
                responseType = 'danger'
            }

            req.params.status="all"
            req.params.category="all"

            let posts = await this.getAllPosts(req,res,next,'return')

            let post_response_json = JSON.parse(posts)

            res.render('posts/all', {
                status:"all",
                category:"all",
                tableData:posts,
                categories:post_response_json.categories,
                responseMessage: responseMessage,
                responseType: responseType,
                user:req.session[config.SESSIONVAR+'_user'].user,
                API_URL:config.API_URL
            });

        }
        else{

            req.params.status="all"
            req.params.category="all"

            let posts = await this.getAllPosts(req,res,next,'return')

            let post_response_json = JSON.parse(posts)

            res.render('posts/all', {
                status:"all",
                category:"all",
                tableData:posts,
                categories:post_response_json.categories,
                responseMessage: res.__("Could not get post to delete,invalid data sent"),
                responseType:"danger",
                user:req.session[config.SESSIONVAR+'_user'].user,
                API_URL:config.API_URL
            });
        }

    }
    catch(error){

        req.params.status="all"
        req.params.category="all"

        let posts = await this.getAllPosts(req,res,next,'return')

        let post_response_json = JSON.parse(posts)

        res.render('posts/all', {
            status:"all",
            category:"all",
            tableData:posts,
            categories:post_response_json.categories,
            responseMessage: res.__("Internal server error, please contact support"),
            responseType:"danger",
            user:req.session[config.SESSIONVAR+'_user'].user,
            API_URL:config.API_URL
        });
    }
}

/**
 * get post data for view post page
 * @param {id} req 
 * @param {*} res 
 * @param {*} next 
 */

exports.viewPost = async (req, res, next) => {

    try{

        let postid = req.params.id

        if(postid){

            let response = await fetch(config.API_URL+"/posts/getProductDetail",{
                method:'POST',
                async:false,
                body:JSON.stringify({postId:postid}),
                headers: {'client_base_url':config.BASE_URL,'content-type': 'application/json', 'Authorization': 'Bearer '+req.session[config.SESSIONVAR+'_token'] }
            });

            let status = response.status

            //if we get 403, it means our session is expired, lets renew it
            if(response.status===403){

                await auth_methods.refreshPermissions(req,res)

                return await this.viewPost(req, res, next);

            }

            let data = await response.json()

            let responseMessage = ''
            let responseType = ''

            if(status===200){
                responseMessage = "";
                responseType = ''
            }
            else{
                let message = res.__('OPERATION_UNAUTHORIZED_MESSAGE');    
                message = message.replace("{$1}",res.__('id_read_capital'))

                responseMessage = message
                responseType = 'danger'
            }

            res.render('posts/view', {
                data:data,
                responseMessage:responseMessage,
                responseType:responseType,
                user:req.session[config.SESSIONVAR+'_user'].user,
                API_URL:config.API_URL
            });

        }
        else{
            res.render('posts/view', {
                responseMessage: res.__("Could not get post data,invalid data sent"),
                responseType:"danger",
                user:req.session[config.SESSIONVAR+'_user'].user,
                API_URL:config.API_URL
            })
        }

    }
    catch(error){
        res.render('posts/view', {
            responseMessage: res.__("Could not get post data,invalid data sent"),
            responseType:"danger",
            user:req.session[config.SESSIONVAR+'_user'].user,
            API_URL:config.API_URL
        })
    }

    
}

/**
 * create new post
 * @param {post details} req 
 * @param {*} res 
 * @param {*} next 
 */

exports.createNewPost = async (req, res, next) => {
    try{
        if(req.method === 'POST')
        {

            let description = req.body.description.trim()

            let files = req.body.files_added_links

            let media = []

            if(files && files!==""){

                //we have to create our media array

                let media_links = req.body.files_added_links
                let media_types = req.body.files_added_links_types

                let media_links_splitted = media_links.split(",");
                let media_types_splitted = media_types.split(",");

                for(let media_i=0;media_i<media_links_splitted.length;media_i++){

                    let inner_array = {
                        URL:media_links_splitted[media_i],
                        Type:media_types_splitted[media_i]
                    }

                    media.push(inner_array)

                }

            }

            let titleError = '', descriptionError = '', categoryError = ''
            if(req.body.title.trim() === '')
                titleError = 'Title required'
            if(description.trim() === '')
                descriptionError = 'Description required'
            if(!req.body.hasOwnProperty('category') || ( req.body.hasOwnProperty('category') && !Array.isArray(req.body.category)) || (Array.isArray(req.body.category) && !req.body.category.length)){
                categoryError = 'Category required'
            }

            let category = []

            //multiple select gives us diff type of data, i.e. array in case of multiple selections, string if only one is selected

            if(typeof req.body.category === 'string'){
                category.push(req.body.category)
            }
            else{
                category = req.body.category
            }

            let post = {
                id: req.body.id,
                title: req.body.title,
                description: description,
                media:media,
                category:category,
                status: req.body.status,
            }

            let response = ''
            if(post.id === '' || post.id === undefined || post.id === null){
                
                response = await fetch(config.API_URL+"/posts/create",{
                    method:'POST',
                    async:false,
                    body:JSON.stringify({post:post}),
                    headers: {'client_base_url':config.BASE_URL,'content-type': 'application/json', 'Authorization': 'Bearer '+req.session[config.SESSIONVAR+'_token'] }
                });
                // REDIRECT USER TO CREATE POST PAGE WITH ERRORS
                let status = response.status
                
                //if we get 403, it means our session is expired, lets renew it
                if(response.status===403){

                    await auth_methods.refreshPermissions(req,res)

                    return await this.createNewPost(req, res, next);

                }

                let responseMessage = ''
                let responseType = ''

                if(status===200){
                    responseMessage = res.__("Post has been created successfully");
                    responseType = 'success'

                    let data = await response.json()

                    //response must also have categories

                    post.categories = data.post.categories
                    post.postcategories = data.post.postcategories

                    //when post is published, it should redirect to post details page
                    //but lets put a check that server has returned us id of the post created

                    if(req.body.status==="publish" && data.hasOwnProperty('post')){
                        let post_id = data.post.id
                        return res.redirect('/posts/'+post_id+'/view');
                    }

                }
                else{
                    let message = res.__('OPERATION_UNAUTHORIZED_MESSAGE');    
                    message = message.replace("{$1}",res.__('id_create_capital'))

                    responseMessage = message
                    responseType = 'danger'
                }

                post.API_URL = config.API_URL

                res.render('posts/create', {
                    post: post,
                    responseStatus: status,
                    responseMessage:responseMessage,
                    responseType:responseType,
                    user:req.session[config.SESSIONVAR+'_user'].user,
                    API_URL:config.API_URL
                });
            }
            else{
                
                response = await fetch(config.API_URL+"/posts/update",{
                    method:'POST',
                    async:false,
                    body:JSON.stringify({post:post}),
                    headers: {'client_base_url':config.BASE_URL,'content-type': 'application/json', 'Authorization': 'Bearer '+req.session[config.SESSIONVAR+'_token'] }
                });

                // REDIRECT USER TO CREATE POST PAGE WITH ERRORS
                let status = response.status

                //if we get 403, it means our session is expired, lets renew it
                if(response.status===403){

                    await auth_methods.refreshPermissions(req,res)

                    return await this.createNewPost(req, res, next);

                }

                let responseMessage = ''
                let responseType = ''

                if(status===200){

                    //response must also have categories

                    let update_response_data = await response.json()

                    post.categories = update_response_data.categories
                    post.postcategories = update_response_data.postcategories

                    responseMessage = res.__("Post has been updated successfully");
                    responseType = 'success'
                    
                    //when post is published, it should redirect to post details page
                    if(req.body.status==="publish"){
                        return res.redirect('/posts/'+post.id+'/view');
                    }

                }
                else{
                    let message = res.__('OPERATION_UNAUTHORIZED_MESSAGE');    
                    message = message.replace("{$1}",res.__('id_update_capital'))

                    responseMessage = message
                    responseType = 'danger'
                }
                    
                post.API_URL = config.API_URL

                res.render('posts/create', {
                    post: post,
                    responseStatus: status,
                    responseMessage:responseMessage,
                    responseType:responseType,
                    user:req.session[config.SESSIONVAR+'_user'].user,
                    API_URL:config.API_URL
                });
            }

            
        }
        else{
            // GET POST DETAIL FROM API
            if(req.params.id !== undefined){
                // UPDATE POST OBJECT
                await getPostById(req.params.id, req.session[config.SESSIONVAR+'_token'])
            }
            else{
                // EMPTY POST OBJECT
                await emptyPostObject(req,res)
            }
            // REDIRECT USER TO CREATE POST PAGE
            res.render('posts/create', {post: defaultPost,responseType:'',responseMessage:'',user:req.session[config.SESSIONVAR+'_user'].user,API_URL:config.API_URL});
        }
    }
    catch(error){
        console.log('error')
        console.log(error)
    }
};

/**
 * get post data by it's id
 * @param {id} id 
 * @param {*} token 
 */

async function getPostById(id,token){
    let response = await fetch(config.API_URL+"/posts/getDetail",{
        method:'POST',
        async:false,
        body:JSON.stringify({postId:id}),
        headers: {'client_base_url':config.BASE_URL,'content-type': 'application/json', 'Authorization': 'Bearer '+token }
    });

    //if we get 403, it means our session is expired, lets renew it
    if(response.status===403){

        await auth_methods.refreshPermissions(req,res)

        return await this.getPostById(id,token);

    }

    let result = await response.json()

    result.form_action = "/posts/"+id+"/edit"
    result.API_URL = config.API_URL
    defaultPost = result
}
async function emptyPostObject(req,res){
    
    let response = await fetch(config.API_URL+"/categories",{
        method:'GET',
        async:false,
        headers: {'client_base_url':config.BASE_URL,'content-type': 'application/json', 'Authorization': 'Bearer '+req.session[config.SESSIONVAR+'_token'] }
    });

    let data = await response.json()

    defaultPost = {
        id: '',
        title: '',
        description: '',
        media:[],
        categories:data.categories,
        postcategories:[],
        status: '',
        responseMessage: '',
        responseType:'',
        API_URL:config.API_URL
    }
}