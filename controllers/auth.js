const fetch = require("node-fetch");
const config = require('../config')
const jsonwebtoken = require('jsonwebtoken')
var CryptoJS = require("crypto-js");

module.exports = {

    /**
     * GET USER TOKENS
     * @param {*} req 
     * @param {*} res 
    */

    async getPermissions(req,res){

        try{
            let map={};

            /*
                * lets check authentication method
                * if it is config then pick values from config
                * if it is post then get values from post
            */

            if(config.authentication_method=="POST"){
                //we will get values from POST

                let data = req.body;
                //let validate this data
                if(!data.hasOwnProperty('app_key_hash')){
                    res.redirect('/error');
                }

                if(!data.hasOwnProperty('module_id')){
                    res.redirect('/error');
                }

                map['module_id'] = data.module_id
                map['app_key'] = data.app_key_hash
            }
            else if(config.authentication_method=="CONFIG"){
                //we will get values from config

                let data = {};
                data.public_key = req.body.public_key
                data.private_key = req.body.private_key

                let data_json = JSON.stringify(data);
            
                //create hash key from the API keys (in POST method this hash is posted from dashboard )
                var ciphertext = CryptoJS.AES.encrypt(data_json,config.SALT);
                let app_key_hash = ciphertext.toString()
            
                map['app_key'] = app_key_hash
                //get module name from config, (in POST method this is also posted from the dashboard)
                map['module_id'] = req.body.module_id

            }
            
            map['api'] = 'module_access'
            map['appType'] = 'express'
            let jsonmap = JSON.stringify(map);

            let response = await fetch(config.API_URL,{
                method:'POST',
                async:false,
                body:jsonmap,
                headers: {'client_base_url':config.BASE_URL,'content-type': 'application/json'}
            });
            const ret_data = await response.json();
            
            if(ret_data.token===""){
                if(config.authentication_method=="CONFIG"){
                    res.render('login', {
                        responseType:'danger',
                        responseMessage:res.__('Invalid credentials'),
                        API_URL:config.API_URL
                    });
                }
                else{
                    res.redirect('/error');
                }
            }

            let tokenData = jsonwebtoken.verify(ret_data.token, config.JWTSECRET)
            req.session[config.SESSIONVAR+'_token'] = ret_data.token
            req.session[config.SESSIONVAR+'_refreshToken'] = ret_data.RefreshToken
            req.session[config.SESSIONVAR+'_user'] = tokenData

            if(ret_data.token === ''){
                res.redirect('/error');
            }
            return ''
        }
        catch(error){
            console.log(error)
        }
    },

    /**
     * REFRESH USER TOKEN FROM API WITH THE HELP OF REFRESH TOKEN
     * @param {*} req 
     * @param {*} res 
    */

    async refreshPermissions(req,res){
        try{

            if(req.session.hasOwnProperty(config.SESSIONVAR+'_refreshToken')){
                let map={};
                map['module_id'] = config.module_name
                map['api'] = 'module_access'
                map['key'] = 'refresh'
                map['appType'] = 'express'
                map['refreshToken'] = req.session[config.SESSIONVAR+'_refreshToken']
                let jsonmap = JSON.stringify(map);

                let response = await fetch(config.API_URL,{
                    method:'POST',
                    async:false,
                    body:jsonmap,
                    headers: {'client_base_url':config.BASE_URL,'content-type': 'application/json'}
                });

                const ret_data = await response.json();
                let tokenData = jsonwebtoken.verify(ret_data.token, config.JWTSECRET)
                req.session[config.SESSIONVAR+'_token'] = ret_data.token
                req.session[config.SESSIONVAR+'_refreshToken'] = ret_data.RefreshToken
                req.session[config.SESSIONVAR+'_user'] = tokenData

                if(ret_data.token === ''){
                    res.redirect('/error');
                }
            }
            else{
                res.redirect('/error');
            }
            return ''
        }
        catch(error){
            console.log(error)
        }
    },

    /**
     * show log in page
     * @param {*} req 
     * @param {*} res 
     */

    async login(req,res){

        res.render('login', {
            responseType:'',
            responseMessage:'',
            API_URL:config.API_URL
        });

    },

    /**
     * LOG THE USER OUT OF THE SYSTEM, CLEAR SESSIONS I.E. TOKENS 
     * @param {*} req 
     * @param {*} res 
    */

    async logout(req,res){

        delete req.session[config.SESSIONVAR+'_token'];
        delete req.session[config.SESSIONVAR+'_refreshToken'];
        delete req.session[config.SESSIONVAR+'_user'];
    
        //if authentication method is config, then redirect to login page, otherwise redirect to usermodule

        if(config.authentication_method==='CONFIG'){
            res.redirect(config.BASE_URL+"/login");
        }
        else{
        res.redirect(config.USER_MODULE_URL);
        }
    
    }
}