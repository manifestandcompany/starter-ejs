const config = require('../config');

/**
 * show user the dashboard page
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */

exports.dashboardPage = async (req, res, next) => {
    
    res.render('dashboard', {
        responseType:'',
        responseMessage:'',
        user:req.session[config.SESSIONVAR+'_user'].user,
        API_URL:config.API_URL
    });

};