const fetch = require("node-fetch");
const config = require('../config')
const auth_methods = require('../controllers/auth.js')

module.exports = {

    /**
     * GET SETTINGS
     * @param {*} req 
     * @param {*} res 
    */

    async getSettings(req,res,next,response_type = "render"){

        try{
            
            let response = await fetch(config.API_URL+"/settings/get",{
                method:'POST',
                async:false,
                body:JSON.stringify({}),
                headers: {'client_base_url':config.BASE_URL,'content-type': 'application/json', 'Authorization': 'Bearer '+req.session[config.SESSIONVAR+'_token'] }
            });
            
            //if we get 403, it means our session is expired, lets renew it
            if(response.status===403){
    
                await auth_methods.refreshPermissions(req,res)
    
                return await this.getAllPosts(req, res, next, response_type);
    
            }

            // GET JSON DATA
            const ret_data = await response.json();

            if(response.status===200){
                //all went well
    
                if(response_type==="return"){
                    return ret_data
                }
                else{
                    // SEND DATA WITH STATUS TO SETTINGS PAGE

                    res.render('settings/all', {
                        settings: ret_data.settings,
                        responseType:'success',
                        responseMessage:'',
                        user:req.session[config.SESSIONVAR+'_user'].user,
                        API_URL:config.API_URL
                    });
                }
            }
            else{
                //permission issues
    
                if(response_type==="return"){
                    return ret_data
                }
                else{
                    
                    let message = res.__('OPERATION_UNAUTHORIZED_MESSAGE');
    
                    message = message.replace("{$1}",res.__('id_read_capital'))
        
                    res.render('dashboard', {
                        categories:[],
                        responseType:'danger',
                        responseMessage:message,
                        user:req.session[config.SESSIONVAR+'_user'].user,
                        API_URL:config.API_URL
                    })
                }
    
            }

        }
        catch(error){
            console.log(error.message)
            res.redirect('error')
        }
    },

    /**
     * UPDATE ALL SETTINGS FROM VIEW ALL SETTINGS PAGE
     * @param {*} req 
     * @param {*} res 
     * @param {*} next 
     */

    async updateAllSettings(req,res,next){

        try{

            let settings = req.body
            
            let response = await fetch(config.API_URL+"/settings/updateAll",{
                method:'POST',
                async:false,
                body:JSON.stringify({settings}),
                headers: {'client_base_url':config.BASE_URL,'content-type': 'application/json', 'Authorization': 'Bearer '+req.session[config.SESSIONVAR+'_token'] }
            });

            //if we get 403, it means our session is expired, lets renew it
            if(response.status===403){
    
                await auth_methods.refreshPermissions(req,res)
    
                return await this.updateAllSettings(req, res, next);
    
            }

            // GET JSON DATA
            const ret_data = await response.json();

            if(response.status===200){
                //all went well
    
                let message = res.__('Settings updated successfully');

                return res.send({responseType:'success',responseMessage:message})
                
            }
            else{

                //permission issues
                let message = res.__('OPERATION_UNAUTHORIZED_MESSAGE');    
                message = message.replace("{$1}",res.__('id_manager_capital'))
        
                    return res.send({responseType:'danger',responseMessage:message})

            }

        }
        catch(error){
            console.log(error.message)
            //res.redirect("error")
        }

    },

}