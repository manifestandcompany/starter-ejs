const fetch = require("node-fetch");
const config = require('../config');
const auth_methods = require('../controllers/auth.js')

/**
 * GET ALL COMMENTS FROM API
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 * @param {render||return, default render} response_type 
 */

exports.getAllMedia = async (req,res,next,response_type = "render") => {
    try{

        // SEND AJAX CALL TO GET ALL media
        let response = await fetch(config.API_URL+"/media/get",{
            method:'POST',
            async:false,
            body:JSON.stringify({}),
            headers: {'client_base_url':config.BASE_URL,'content-type': 'application/json', 'Authorization': 'Bearer '+req.session[config.SESSIONVAR+'_token'] }
        });
        
        //if we get 403, it means our session is expired, lets renew it
        if(response.status===403){

            await auth_methods.refreshPermissions(req,res)

            return await this.getAllMedia(req, res, next, response_type);

        }

        // GET JSON DATA
        const ret_data = await response.json();

        if(response.status===200){
            if(response_type==="return"){
                return JSON.stringify(ret_data)
            }
            else{
                // SEND DATA WITH STATUS TO comments PAGE
                res.render('media/all', {
                    tableData: JSON.stringify(ret_data),
                    responseType:'success',
                    responseMessage:'',
                    user:req.session[config.SESSIONVAR+'_user'].user,
                    API_URL:config.API_URL
                });
            }
        }
        else{

            if(response_type==="return"){
                return JSON.stringify(ret_data)
            }
            else{

                let message = res.__('OPERATION_UNAUTHORIZED_MESSAGE');    
                message = message.replace("{$1}",res.__('id_read_capital'))

                res.render('dashboard', {
                    responseType:'danger',
                    responseMessage:message,
                    user:req.session[config.SESSIONVAR+'_user'].user,
                    API_URL:config.API_URL
                })
            }

        }

    }
    catch(error){
        console.log('error')
        console.log(error)
    }
}

/**
 * delete single media physically
 * @param {id} req 
 * @param {*} res 
 * @param {*} next 
 */

exports.deleteMedia = async (req,res,next) => {
    try{
        
        let mediaId = req.params.id

        if(mediaId){
            let response = await fetch(config.API_URL+"/media/delete",{
                method:'POST',
                async:false,
                body:JSON.stringify({mediaId:mediaId}),
                headers: {'client_base_url':config.BASE_URL,'content-type': 'application/json', 'Authorization': 'Bearer '+req.session[config.SESSIONVAR+'_token'] }
            });
            
            let status = response.status

            //if we get 403, it means our session is expired, lets renew it
            if(response.status===403){

                await auth_methods.refreshPermissions(req,res)

                return await this.deleteMedia(req, res, next);

            }

            let responseMessage = ''
            let responseType = ''

            if(status===200){
                responseMessage = res.__("Media has been deleted successfully");
                responseType = 'success'
            }
            else{
                let message = res.__('OPERATION_UNAUTHORIZED_MESSAGE');    
                message = message.replace("{$1}",res.__('id_delete_capital'))

                responseMessage = message
                responseType = 'danger'
            }

            let media = await this.getAllMedia(req,res,next,'return')

            res.render('media/all', {
                tableData:media,
                responseMessage: responseMessage,
                responseType: responseType,
                user:req.session[config.SESSIONVAR+'_user'].user,
                API_URL:config.API_URL
            });

        }
        else{
            let media = await this.getAllMedia(req,res,next,'return')
            res.render('media/all', {
                tableData:media,
                responseMessage: res.__("Could not get media to delete,invalid data sent"),
                responseType:"danger",
                user:req.session[config.SESSIONVAR+'_user'].user,
                API_URL:config.API_URL
            });
        }

    }
    catch(error){

        let media = await this.getAllMedia(req,res,next,'return')

        res.render('media/all', {
            tableData:media,
            responseMessage: res.__("Internal server error, please contact support"),
            responseType:"danger",
            user:req.session[config.SESSIONVAR+'_user'].user,
            API_URL:config.API_URL
        });
    }
}

/**
 * delete multiple posts physically
 * @param {ids} req 
 * @param {*} res 
 * @param {*} next 
 */

exports.deleteMedias = async (req,res,next) => {
    try{

        let body = req.body

        if(body.hasOwnProperty('ids') && body.ids.length){
            let ids = req.body.ids

            let response = await fetch(config.API_URL+"/media/deleteMultipleMedias",{
                method:'POST',
                async:false,
                body:JSON.stringify({ids:ids}),
                headers: {'client_base_url':config.BASE_URL,'content-type': 'application/json', 'Authorization': 'Bearer '+req.session[config.SESSIONVAR+'_token'] }
            });
            
            let status = response.status

            let responseMessage = ''
            let responseStatus = ''

            //if we get 403, it means our session is expired, lets renew it
            if(response.status===403){

                await auth_methods.refreshPermissions(req,res)

                return await this.deleteMedias(req, res, next);

            }

            if(status===200){
                responseMessage = res.__("Selected media have been deleted successfully");
                responseStatus = true
            }
            else{
                let message = res.__('OPERATION_UNAUTHORIZED_MESSAGE');    
                message = message.replace("{$1}",res.__('id_delete_capital'))

                responseMessage = message
                responseStatus = false
            }

            res.send({
                status:responseStatus,
                message:responseMessage
            })

        }
        else{
            res.send({
                status:0,
                message:res.__('Invalid data sent, no records to delete')
            })
        }

    }
    catch(error){
        res.send({
            status:0,
            error:error.message,
            message:res.__('Internal server error, please contact support')
        })
    }
}

/**
 * create single comment on a post
 * @param {postId, comment} req 
 * @param {*} res 
 * @param {*} next 
 */

exports.createComment = async (req,res,next) => {

    try{

        let postid = req.body.postId
        let comment = req.body.comment

        if(postid && comment){

            let post = {
                postId: postid,
                comment: comment,
                status: 'pending',
            }

            let response = await fetch(config.API_URL+"/comments/create",{
                method:'POST',
                async:false,
                body:JSON.stringify({post}),
                headers: {'client_base_url':config.BASE_URL,'content-type': 'application/json', 'Authorization': 'Bearer '+req.session[config.SESSIONVAR+'_token'] }
            });

            let response_status = response.status

            //if we get 403, it means our session is expired, lets renew it
            if(response.status===403){

                await auth_methods.refreshPermissions(req,res)

                return await this.createComment(req, res, next);

            }

            let responseMessage = ''
            let responseType = ''

            if(response_status===200){
                responseMessage = res.__('COMMENT_CREATED_MESSAGE');
                responseType = 'success'
            }
            else{
                let message = res.__('OPERATION_UNAUTHORIZED_MESSAGE');    
                message = message.replace("{$1}",res.__('id_create_capital'))

                responseMessage = message
                responseType = 'danger'
            }

            res.send({
                responseMessage,responseType
            })

        }
        else{

            let responseType = 'danger'

            let responseMessage = res.__("Could not create media,invalid data sent")

            res.send({
                responseMessage,responseType
            })

        }

    }
    catch(error){

        let responseType = 'danger'

        let responseMessage = res.__("Internal server error, please contact support")

        res.send({
            responseMessage,responseType
        })
        
    }
}
