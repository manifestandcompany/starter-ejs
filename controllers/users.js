const fetch = require("node-fetch");
const config = require('../config');

/**
 * show users on the dashboard page
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */

exports.getAllUsers = async (req, res, next) => {
    
     // SEND AJAX CALL TO GET ALL POSTS
    let response = await fetch(config.API_URL+"/users",{
        method:'POST',
        async:false,
        body:JSON.stringify({}),
        headers: {'client_base_url':config.BASE_URL,'content-type': 'application/json', 'Authorization': 'Bearer '+req.session[config.SESSIONVAR+'_token'] }
    });
    
    res.render('users-dashboard', {
        responseType:'',
        responseMessage:'',
        user:req.session[config.SESSIONVAR+'_user'].user,
        data: response,
        API_URL:config.API_URL
    });
    console.log(response);
};